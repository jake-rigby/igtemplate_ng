
module.exports = function (gulp, manifest, namespace, debug) {

	console.log(manifest, namespace, debug);


	var browserify = require('browserify'),
		transform = require('vinyl-transform'),
		concat = require('gulp-concat'),
		uglify = require('gulp-uglify'),
		del = require('del'),
		//concatcss = require('gulp-concat-css'), // <-- don't use concatcss as this will rebase fontawsome urls
		uglifycss = require('gulp-uglifycss'),
		mainBowerFiles = require('main-bower-files'),
		ngAnnotate = require('gulp-ng-annotate'),
		filter = require('gulp-filter'),
		fs = require('fs-extra'),
		replace = require('gulp-replace'),
		runsequence = require('run-sequence'),
		flatten = require('flat'),
		rename = require('gulp-rename'),
		sass = require('gulp-sass'),
		jeditor = require('gulp-json-editor'),
		merge = require('merge2'),
		source = require("vinyl-source-stream");
		babelify = require('babelify'),
		babel = require('gulp-babel'),
		path = require('path'),
		ifElse = require('gulp-if-else');

	gulp.task('browserify', function () {
		var browserified = transform(function (filename) { // http://stackoverflow.com/questions/24329690/how-to-expose-require-to-the-browser-when-using-browserify-from-within-gulp
	 		var moduleName = path.basename(filename, '.js')
	 		var b = browserify(filename)
	 		b.transform(babelify, {presets: ["es2015"]});
			b.require(filename, {expose:moduleName}); // namespace
			return b.bundle();
		})
		return gulp.src('./src/shared/**/*')
			.pipe(browserified)
			.pipe(ifElse(!debug, uglify))
			.pipe(gulp.dest('browserified'))
	})

	gulp.task('html', function () {
		return gulp.src(manifest.static.html)
			.pipe(gulp.dest('build/www'))
	})

	gulp.task('fonts', function (cb) {
	    return gulp.src(manifest.static.fonts)
	        .pipe(gulp.dest('build/www/fonts'));
	})

	gulp.task('images', function () {
		return gulp.src(manifest.static.images)
			.pipe(gulp.dest('build/www/images'));
	})

	gulp.task('images-deps', function () {
		return gulp.src(mainBowerFiles())
			.pipe(filter(['*.png','*.gif','*.jpg']))
			.pipe(gulp.dest('build/www/images'))
	})

	gulp.task('html-deps', function () {
		return gulp.src(mainBowerFiles())
			.pipe(filter('*.html'))
			.pipe(gulp.dest('build/www'));
	})

	gulp.task('css-deps', function () {
		return merge(
				gulp.src(mainBowerFiles()
					.concat(manifest.static.css_deps) )
					.pipe(filter('*.css')),
				gulp.src(manifest.static.scss_deps)
					.pipe(sass({
						precision: 10,
						includePaths:manifest.static.scss_deps
					}).on('error', function (err) {
						console.error('SASS ERROR: '+err);
					}))
			)
			.pipe(concat(namespace+'.deps.dev.css')) // <-- don't use concatcss as this will rebase fontawsome urls
			.pipe(gulp.dest('build/www/css'));
	})

	gulp.task('js-deps', function () {
		if (debug) return gulp.src(mainBowerFiles().concat(manifest.static.js_deps))
			.pipe(filter('*.js'))
			.pipe(concat(namespace+'.deps.dev.js'))
			.pipe(gulp.dest('build/www/js'));
		else return gulp.src(mainBowerFiles().concat(manifest.static.js_deps))
			.pipe(filter('*.js'))
			.pipe(uglify())
			.pipe(concat(namespace+'.deps.dev.js'))
			.pipe(gulp.dest('build/www/js'));
	})

	gulp.task('js', ['browserify'], function () {
        return gulp.src(manifest.static.js)
		.pipe(ngAnnotate())
        .pipe(babel({presets:[
    		['es2015', {modules: false}]
		]}))
        .pipe(concat(namespace+'.dev.js'))
		.pipe(ifElse(!debug, uglify))
        .pipe(gulp.dest('build/www/js'))
        .on('error', function(err) {
            console.error(err);
        })
    })

	gulp.task('css', function() {
		return merge(
				gulp.src(manifest.static.css)
					.pipe(filter('*.css')),
				gulp.src(manifest.static.scss)
					.pipe(sass({
						precision: 10,
						includePaths:manifest.static.scss_deps
					}).on('error', function (err) {
						console.error('SASS ERROR: '+err);
					}))
			)
			.pipe(concat(namespace+'.dev.css')) // <-- don't use concatcss as this will rebase fontawsome urls
			.pipe(gulp.dest('build/www/css'));
	})

	gulp.task('server', function () {
		return gulp.src(manifest.server)
			.pipe(gulp.dest('build'));
	})
}