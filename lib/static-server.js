
function Server () {

	var express = require('express'),
		http = require('http');

	this.ipaddress 	= 	process.env.OPENSHIFT_NODEJS_IP;
	this.port 		= 	process.env.OPENSHIFT_NODEJS_PORT
					||	process.env.PORT
					||	8080;

	if (this.ipaddress === undefined) {
		console.warn('No OPENSHIFT_NODEJS_IP var, using 127.0.0.1');
		this.ipaddress = '127.0.0.1';
	}

	// Exit gracefully in response to POSIX signals
	function terminate (sig) {
		if (typeof sig === 'string') {
			console.log('%s : App terminated with signal %s', Date(Date.now()), sig);
			process.exit(1);
		} else {
			console.log('%s : App terminated', Date(Date.now()));
		}
	}

	process.on('exit', function() { terminate(); });

	// Removed 'SIGPIPE' from the list - bugz 852598.
	[
		'SIGHUP', 'SIGINT', 'SIGQUIT', 'SIGILL', 'SIGTRAP', 'SIGABRT',
		'SIGBUS', 'SIGFPE', 'SIGUSR1', 'SIGSEGV', 'SIGUSR2', 'SIGTERM'
	].forEach(function (element, index, array) {
		process.on(element, function() { terminate(element); });
	});

	this.app = express();
	this.server = http.createServer(this.app);
	this.express = express;

}

Server.prototype.start = function(cb) {
	this.server.listen(this.port, this.ipaddress, function () {
		cb(null, Date(Date.now)+' : Started server on '+this.ipaddress+':'+this.port);
	}.bind(this))	
};

module.exports.Server = Server;


// e.g. static

// server.app.use(require('express').static('webclient/www'));



// e.g. route (use for services)

// server.app.get('/myservice', function (req, res) {
// 	res.setHeader('Content-Type', 'text/html');
// 	res.sendfile('index.html', {root:'./webclient/www'});
// })


