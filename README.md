## Notes

### manifest.json
Mostly about static sources for the build products. The *_deps entries are for anything not taken care of by main-bower-files. Bear in mind that `/**/*` globs will preserve ancestor directory structure, but all manifest entries in the same block will be siblings

### .openshift
private bitbucket dependencies are removed from dist/package.json and added to the .openshift/action_hooks/deploy script

## Building
`build/` and `dist/` are ignored by default. `build/` is an intermediary step befor the configs are piped in.

`sh init.sh <namespace>`
Strip out the .git info and copy the project into a sibling directory. `namespace` names the directory and as a replacer in the configuration files. You can then push -u to a new remote.

`sh test.sh`
Build the sources in *monifest.json* then populate with *config-local.json* 