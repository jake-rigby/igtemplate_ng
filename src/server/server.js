var server = new (require('./static-server').Server)();
server.app.use(server.express.static('./www'));
server.start(console.log);

var bodyParser = require('body-parser');

server.app.use(bodyParser.urlencoded({ extended: false }), bodyParser.json());
server.app.use(bodyParser.text());

server.app.get('/echo/:message', function (req, res) {
	res.send('ECHO: '+req.params.message);
})