
// Module definition

angular.module('$igtemplate-ns|', [
	'ui.router', 
	'utils', 
	'slideout', 
	'btford.modal', 
	'jsonFormatter'
])


// Route configuration

.config(function ($stateProvider, $urlRouterProvider) {

	$stateProvider
	
	.state('home', {
		url: '/',
		views: {
			'header': {
				templateUrl: 'header.html',
				controller: 'HeaderCtrl'
			},
			'content': {
				template: '<section>\
								<div class="items">\
									<item ng-repeat="item in items" remove="remove(item)" select="select(item)" class="example">\
										<left>{{item.name}}</left>\
										<right>{{item.description}}</right>\
										<bottom>{{item.date}}</bottom>\
									</item>\
								</div>\
							</section>',
				controller: 'ItemsCtrl'
			},
			'nav': {
				templateUrl: 'menu.html',
				controller: 'NavCtrl'
			},
			'action': {
				controller: 'ActionCtrl',
				template: '<div class="action-button" active="true" ng-click="action()">\
							 <i class="fa fa-plus"></i>\
						   </div>'
			}
		}
	})

	.state('json', {
		url: '/json',
		views: {
			'header': {
				templateUrl: 'header.html',
				controller: 'HeaderCtrl'
			},
			'content': {
				template: '<section>\
								<object-view content="items" open="1" ></object-view>\
							  </section>',
				controller: 'ItemsCtrl'
			},
			'nav': {
				templateUrl: 'menu.html',
				controller: 'NavCtrl'
			}
		}
	})

	.state('dave', {
		url: '/dave',
		views: {
			'header': {
				templateUrl: 'header.html',
				controller: 'HeaderCtrl'
			},
			'content': {
				templateUrl: 'dave.html',
				controller: 'MsgCtrl'
			},
			'nav': {
				templateUrl: 'menu.html',
				controller: 'NavCtrl'
			}
		}
	})

	$urlRouterProvider.otherwise('/');
})


// Primary Action

.controller('ActionCtrl', function ($scope, $rootScope) {
	$scope.action = function (data) {
		$rootScope.$emit('ACTION', data);
	}
})


// Navigation	

.controller('HeaderCtrl', function ($scope, $state) {
	$scope.title = $state.current.name;
})

.controller('NavCtrl', function ($scope, $state, SlideoutSvc) {

	$scope.options = [
		{
			label: 'home',
			fnc: function () {
				SlideoutSvc.close();
				$state.go('home');
			}
		},
		{
			label: 'jason!',
			fnc: function () {
				SlideoutSvc.close();
				$state.go('json');
			}
		},
		{
			label: 'dave',
			fnc: function () {
				SlideoutSvc.close();
				$state.go('dave');
			}
		}
	]
})


// A data model

.value('items', [ // some bullshit example data
	{
		name: '1',
		description: 'one',
		date: new Date(1978,1,25,10,20)
	},
	{
		name: '2',
		description: 'two',
		date: new Date(2001,11,9,9,15)
	},
	{
		name: '1',
		description: 'one',
		date: new Date(1978,1,25,10,20)
	},
	{
		name: '2',
		description: 'two',
		date: new Date(2001,11,9,9,15)
	}
])


// a modal definition with an inline template

.factory('newItemModal', function (btfModal) {
	return btfModal({
		controller: function ($scope, items, newItemModal) {
			$scope.close = newItemModal.deactivate;
			$scope.submit = function () {
				var item = {
					name: $scope.name,
					description: $scope.description,
					date: $scope.date
				}
				items.push(item);
				newItemModal.deactivate();
			}
			$scope.cancel = newItemModal.deactivate;
		},
		controllerAs: 'ctrl',
		template: '<div class="modal-wrapper">\
					<div class="modal card white">\
						<div class="modal-header" ><i class="modal-close fa fa-times" ng-click="close()"></i></div>\
						<div class="modal-content">\
							<form>\
								<div class="row">\
									<div class="twelve columns">\
										<label for="title">Name</label>\
										<input type="text" id="name" ng-model="name">\
										<label for="title">Description</label>\
										<input type="text" id="description" ng-model="description">\
										<label for="date">Date</label>\
										<input type="date" id="date" ng-model="date">\
									</div>\
								</div>\
							</form>\
						</div>\
						<div class="modal-footer text-right">\
							<div class="btn active" ng-click="submit()" id="submit"><i class="fa fa-send"></i></div>\
							<div class="btn active" ng-click="cancel()" id="cancel"><i class="fa fa-times"></i></div>\
						</div>\
					</div>\
				</div>'
	});
})


// Home route

.controller('ItemsCtrl', function ($scope, items, newItemModal, $rootScope) {
	$scope.$on('$destroy', 
		$rootScope.$on('ACTION', newItemModal.activate)
	);
	$scope.$watchCollection(function () { return items; } , items => $scope.items = items);
})


// Dave route

.controller('MsgCtrl', function ($scope, $http) {
	$scope.send = function (message) {
		$http.get('$igtemplate-server.url|/echo/'+message)
		.then(function (res) {
			$scope.response = res.data;
		})		
	}
})
