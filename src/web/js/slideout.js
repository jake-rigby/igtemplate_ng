
angular.module('slideout', [])

.factory('SlideoutSvc', function () {
	var slideout = new Slideout({
		'panel': document.getElementById('slideout-panel'), // bad - these need to be $document so make a service
		'menu': document.getElementById('slideout-menu'),
		'padding': 256,
		'tolerance': 70
	})
	return slideout;
})

.controller('SlideoutCtrl', function ($scope, SlideoutSvc) {
	$scope.open = function () {
		SlideoutSvc.open;
	}
	$scope.close = function () {
		SlideoutSvc.close;
	}
	$scope.toggle = function () {
		SlideoutSvc.toggle();
	}
})
