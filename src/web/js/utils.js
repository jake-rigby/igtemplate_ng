
angular.module('utils', [])


.directive('item', function () {
	return {
		template: '<div class="item">\
					<div id="top">\
						<div ng-transclude="left" id="left">\
						</div>\
						<div ng-transclude="right" id="right">\
						</div>\
					</div>\
					<div ng-transclude="bottom" id="bottom">\
					</div>\
				   </div>',
		transclude: {
			left: 'left',
			right: 'right',
			bottom: 'bottom'
		},
		replace: true
	}
})

// e.g.
// <dot-selector options="['a','b','c']" selected="slct"></dot-selector>
// <div>You selected: {{slct}}</div>
.directive('dotSelector', function () {
	return {
		restrict:'E',
		scope: {
			options: '=',
			selected: '='
		},
		template: '<div class="dot-selector">\
					<ul class="dotstyle dotstyle-scaleup" style="display:inline-block;" ng-show="options.length > 1">\
						<li ng-repeat="o in options" ng-class="selected === o ? \'current\' : \'\'"><a ng-click="select(o)"></a></li>\
					</ul>\
				   </div>',
		link: function (scope) {
			scope.$watch('selected', function (selected) {
				console.log(selected);
			})
			scope.select = function (o) {
				scope.selected = o;
			}
		}
	}
})

.directive('ngEnter', function() {
	return function (scope, element, attrs) {
		element.bind("keydown keypress", function(event) {
			if(event.which === 13) {
					scope.$apply(function(){
							scope.$eval(attrs.ngEnter);
					});
					event.preventDefault();
			}
		});
	};
})

//
.directive('objectView', function () {
	return {
		template: 	'<div class="object-view"><div class="json-wrapper card">'
				+		'<json-formatter open="open" json="content" ></json-formatter>'
				+	'</div></div>',
		scope: {
			content: '=',
			open: '='
		}
	}
})
