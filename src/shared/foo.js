module.exports = class Foo {
	constructor(options) {
		this.options = options;
	}
	render() {
		var a = ['foo','two','c'];
		console.log(a.reduce((r, n) => r + n));
		return this.options.toString();
	}
}
