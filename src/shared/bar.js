module.exports = class Bar {
	constructor(options) {
		this.options = options;
	}
	render() {
		var a = ['bar','two','c'];
		console.log(a.reduce((r, n) => r + n));
		return this.options.toString();
	}
}
